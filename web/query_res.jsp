<%--
  Created by IntelliJ IDEA.
  User: beart
  Date: 6/3/2020
  Time: 1:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="query_aut_style.css">
    <title>Query Result</title>
</head>
<body>
<div class="topnav">
    <a href="index.html">Home</a>
    <a href="update.html">Update</a>
    <a href="query.html">Query</a>
</div>
<h1>
    Query Results
</h1>


<div id="pub-div">
    <p class="input">
        <label class="title"> Publication Name:
        </label>
        ${QueryRes.get("Name")}
    </p>
</div>

<div id="type-div">
    <p class="input">
        <label class="title"> Publication Type:
        </label>
        ${QueryRes.get("Type")}
    </p>
</div>

<c:if test="${not empty AuthorList}">

        <div id="author-div">


                    <p class="title">Authors' names:</p>
            <br>
            <ul>

                    <c:forEach items="${AuthorList}" var="element">
                        <li>
                        <form method="post" action="Query">

                                <input name="hasAuthor" value="${element}" style="display:none !important;">
                                <input type="submit" value="${element}">

                        </form>
                        </li>


                    </c:forEach>
            </ul>


        </div>
</c:if>

<c:if test="${not empty QueryRes.hasYear}">
    <div id="year-div">
        <p class="input">
        <label class="title">Publication Year:</label>
                ${QueryRes.get("hasYear")}
        </p>
    </div>
</c:if>

<c:if test="${not empty QueryRes.hasJournal}">
    <div id="journal-div">
        <p class="input">
        <label class="title">Journal:</label>
                ${QueryRes.get("hasJournal")}
        </p>
    </div>
</c:if>

<c:if test="${ not empty QueryRes.hasBooktitle}">
    <div id="booktitle-div">
        <p class="input">
        <label class="title">Booktitle: </label>
                ${QueryRes.get("hasBooktitle")}
        </p>

    </div>
</c:if>




</body>
</html>
