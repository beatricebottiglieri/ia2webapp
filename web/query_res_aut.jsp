<%--
  Created by IntelliJ IDEA.
  User: beart
  Date: 6/3/2020
  Time: 1:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="query_aut_style.css">
    <title>Query Result</title>
</head>
<body>
<div class="topnav">
    <a href="index.html">Home</a>
    <a href="update.html">Update</a>
    <a href="query.html">Query</a>
</div>
<h1>
    Query Results
</h1>


<div id="aut-div">
    <p class="input">
        <label class="title"> Author's Name: </label>
        ${QueryRes.get("Name")}
    </p>
</div>

<div id="pub-div">
    <p class="title"> Publications: </p>
    <br>
    <ul>
<c:forEach items="${list}" var="element">
    <li>
    <form method="post" action="Query">


                <input name="Publication" value="${element}" style="display:none !important;">
                <input type="submit" value="${element}">


    </form>
    </li>
</c:forEach>
    </ul>
</div>







</body>
</html>
