import org.eclipse.rdf4j.model.Model;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@WebServlet(
        name = "querypubservlet",
        urlPatterns = "/QueryPub"
)
public class QueryPubServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String path = req.getServletContext().getRealPath("/");
        path = path.replace("\\","/");
        String[] apps = path.split("/");
        path = "";
        int i =0;
        while(i<apps.length-3){
            if(i==0){
                path = apps[i];
                i+=1;
            }
            else{
                path = path+"/"+apps[i];
                i+=1;
            }

        }
        //System.out.println(path);

        GestoreRepo gestoreRepo = new GestoreRepo(path);
        Model model = gestoreRepo.getModelFromQuery("PREFIX bibo:<http://purl.org/ontology/bibo/>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "SELECT ?pub \n" +
                "WHERE {\n" +
                "    {?pub rdf:type bibo:AcademicArticle.} UNION\n" +
                "    {?pub rdf:type bibo:Proceedings.} UNION\n" +
                "    {?pub rdf:type bibo:Inproceedings.} UNION\n" +
                "    {?pub rdf:type bibo:Conference.}\n" +
                "    \n" +
                "} ORDER BY DESC(?pub)");


        gestoreRepo.getRdfFile(model, "Complete");
        //long startTime = System.nanoTime();
        gestoreRepo.generateHTMLFromOntology(model);


        //gestoreRepo.closeRepo();
        //System.out.println(System.nanoTime() - startTime);
        //RequestDispatcher view = req.getRequestDispatcher("PublicationList.html");
        //view.forward(req, resp);

        resp.sendRedirect("PublicationList.html");

    }

}
