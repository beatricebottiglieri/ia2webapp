import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {


        GestoreRepo gestoreRepo = new GestoreRepo("D:/IdeaProjectUltimate/ContentNegTest2");


        RepositoryConnection repCon = gestoreRepo.getRep().getConnection();

        Model model = gestoreRepo.getModelFromQuery("PREFIX bibo:<http://purl.org/ontology/bibo/>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "SELECT ?pub \n" +
                "WHERE {\n" +
                "    {?pub rdf:type bibo:AcademicArticle.} UNION\n" +
                "    {?pub rdf:type bibo:Proceedings.} UNION\n" +
                "    {?pub rdf:type bibo:Inproceedings.} UNION\n" +
                "    {?pub rdf:type bibo:Confernece.}\n" +
                "    \n" +
                "}");

        gestoreRepo.generateHTMLFromOntology(model);


        /*
        //gestoreRepo.insertStatement("Fra Cazzo", null, "Agent");
        //gestoreRepo = new GestoreRepo();



        //Model model = gestoreRepo.askQuery(null, "hasAuthor", "Fra Cazzo");

        Model model = gestoreRepo.askQuery(null, "hasAuthor", "Fra Cazzo");

        for(Statement s : model){
            System.out.println(s);
        }

        //gestoreRepo.getRdfFile(model);
        //gestoreRepo.generateHTML("D:/IdeaProjectUltimate/ContentNegTest2/temp/temp.rdf");



        GestoreRepo gestoreRepo = new GestoreRepo();

        Model model = gestoreRepo.askQuery(null, "hasAuthor", "Fra Cazzo");

        for(Statement s : model){
            System.out.println(s);
        }
        */
        






    }

}
