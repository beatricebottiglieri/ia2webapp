import org.eclipse.rdf4j.model.Model;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@WebServlet(
        name = "updateservlet",
        urlPatterns = "/InsertPublication"
)

public class UpdateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Ho bisogno anche di resp per mandare il messaggio di risposta
        int insert = insertData(req, resp);

        String msg = null;
        if(insert == 0) {
            msg = "Inserimento avvenuto con successo!";
        }
        else if(insert == -1){

            //RequestDispatcher view = req.getRequestDispatcher("update.html");
            //view.forward(req, resp);
            //msg = "Inserimento fallito!";
            return;
        }

        ArrayList out = new ArrayList();
        out.add(msg);
        req.setAttribute("message", out);
        RequestDispatcher view = req.getRequestDispatcher("update_result.jsp");
        view.forward(req, resp);


    }

    protected int insertData(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String path = req.getServletContext().getRealPath("/");
        path = path.replace("\\","/");
        String[] apps = path.split("/");
        path = "";
        int i =0;
        while(i<apps.length-3){
            if(i==0){
                path = apps[i];
                i+=1;
            }
            else{
                path = path+"/"+apps[i];
                i+=1;
            }

        }
        //System.out.println(path);

        GestoreRepo gestoreRepo = new GestoreRepo(path);

        Model queryUpdate = null;

        //req.getParameterMap().forEach((key, value) -> System.out.println(key + ":" + Arrays.asList(value)));

        //System.out.println("\n UPDATE \n");

        String name = "";

        for (Map.Entry entry : req.getParameterMap().entrySet()) {

            //salvo la chiave
            String type = (String) entry.getKey();
           // System.out.println(type);
            //salvo il valore e lo salvo
            String[] value_a = (String[]) entry.getValue();
            List<Object> value = Arrays.asList(value_a);
            //System.out.println(value.get(0));




            //inserimento AcademicArticle
            if( ( type.equals("AcademicArticle") || type.equals("Inproceedings") || type.equals("Proceedings")
                    || type.equals("Conference") ) && value.get(0) !=""){
                //inserisci titolo autonomamente

                //mi salvo il nome
                name = (String) value.get(0);

                //verifico che non sia gia presente
                queryUpdate = gestoreRepo.askQuery(name, null, null);

                //se non e' presente
                if (queryUpdate.isEmpty()) {
                    //aggiungo uno statment di tipo nome isa novel/poem etc
                    gestoreRepo.insertStatement(name, null, type);
                    //aggiungo uno statement di tipo nome hasTitle nome
                    gestoreRepo.insertData(name, "hasTitle", name);
                }

                else{
                    //se e' presente mostro un messaggio di errore
                    PrintWriter out = resp.getWriter();
                    resp.setContentType("text/html");
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('La pubblicazione «" + name + "» è già presente');");
                    out.println("window.location.href=\"update.html\";");
                    out.println("</script>");
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return -1;
                }

                //altrimenti non faccio nulla
                if (queryUpdate == null) {
                    gestoreRepo.closeRepo();
                    return -1;
                }
                queryUpdate = null;

            }


            //inserimento hasAuthor
            else if(type.equals("hasAuthor") && value.get(0) !=""){

                //System.out.println("SONO IN AUTHOR");

                //salvo il valore
                String authors = (String) value.get(0);

                //System.out.println(authors);
                //System.out.print(name);

                String[] authorList = gestoreRepo.getAuthorsList(authors);

                for(String author: authorList) {

                    //aggiungo uno statement di tipo nome hasTitle nome
                    Model app = gestoreRepo.askQuery(author, null, null);
                    if (app.isEmpty()) {
                        gestoreRepo.insertStatement(author, null, "Agent");
                    }
                    gestoreRepo.insertStatement(name, "hasAuthor", author);

                }

            }

            //inserimento hasYear
            else if(type.equals("hasYear") && value.get(0) !=""){

                //salvo il valore
                String year = (String) value.get(0);

                //aggiungo uno statement di tipo nome hasTitle nome
                gestoreRepo.insertData(name, "hasYear", year);

            }

            //inserimento hasBookTitle
            else if(type.equals("hasBookTitle") && value.get(0) !=""){

                //salvo il valore
                String booktitle = (String) value.get(0);

                //aggiungo uno statement di tipo nome hasYear year
                gestoreRepo.insertData(name, "hasBooktitle", booktitle);

            }

            //inserimento hasJournal
            else if(type.equals("hasJournal") && value.get(0) !=""){

                //salvo il valore
                String journal = (String) value.get(0);

                //verifico che non sia gia presente
                queryUpdate = gestoreRepo.askQuery(journal, null, null);

                //se non e' presente
                if (queryUpdate.isEmpty()) {
                    //aggiungo uno statment di tipo nome isa journal
                    gestoreRepo.insertStatement(journal, null, "Journal");
                    queryUpdate = null;
                }


                //aggiungo uno statement di tipo nome hasJournal journal
                gestoreRepo.insertStatement(name, "hasJournal", journal);


            }
            queryUpdate = null;
            //name = null;

        }



        gestoreRepo.closeRepo();
        return 0;


    }


}
