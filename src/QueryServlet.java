import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet(
        name = "queryservlet",
        urlPatterns = "/Query"
)

public class QueryServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher view = null;

        String path = req.getServletContext().getRealPath("/");
        path = path.replace("\\","/");
        //System.out.println(path);
        String[] app = path.split("/");
        path = "";
        int i =0;
        while(i<app.length-3){
            while(i<app.length-3){
                if(i==0){
                    path = app[i];
                    i+=1;
                }
                else{
                    path = path+"/"+app[i];
                    i+=1;
                }

            }
        }
        //.out.println(path);

        GestoreRepo gestoreRepo = new GestoreRepo(path);

        Model query = null;

        //System.out.println(req.getParameterMap().entrySet());

        List<String> publicationsList = new ArrayList<>();
        publicationsList.add("AcademicArticle");
        publicationsList.add("Inproceedings");
        publicationsList.add("Proceedings");
        publicationsList.add("Conference");

        //System.out.println("\n QUERY \n");

        for (Map.Entry entry : req.getParameterMap().entrySet()) {

            //salvo la chiave
            String type = (String) entry.getKey();
            //System.out.println(type);
            //salvo il valore e lo salvo
            String[] value_a = (String[]) entry.getValue();
            List<Object> value = Arrays.asList(value_a);
            //System.out.println(value.get(0));


            if(type.equals("Publication")){

                //.out.println("SONO IN PUBLICATION");

                //salvo il valore
                String name = (String) value.get(0);

                /*
                for( String pub : publicationsList){
                    query = gestoreRepo.askQuery(name, null, null);
                    if(query!=null){
                        break;
                    }
                }

                 */

                //System.out.println(name);

                if(name.contains("/")){
                    name = gestoreRepo.getLastUriTerm(name);
                }

                query = gestoreRepo.askQuery(name, null, null);

                if(query.isEmpty()){
                    //System.out.println("QUERY IS EMPTY");
                    req.setAttribute("Type","Publication");
                    view = req.getRequestDispatcher("null_res.jsp");
                }
                else {

                    //for (Statement s : query) {
                        //System.out.println(s);
                    //}


                    gestoreRepo.getRdfFile(query, "Publication");

                    HashMap out = gestoreRepo.getMapFromModel(query);

                    //gestoreRepo.generateHTML("D:/IdeaProjectUltimate/ContentNegTest2/temp/temp.rdf");

                    //System.out.println(out.get("hasAuthor"));

                    req.setAttribute("QueryRes", out);
                    req.setAttribute("AuthorList",out.get("hasAuthor"));
                    view = req.getRequestDispatcher("query_res.jsp");

                    //resp.sendRedirect("query_res.jsp");
                }
                break;



            }

            else if(type.equals("hasAuthor")){

                //System.out.println("SONO IN AUTHOR");

                //salvo il valore
                String author = (String) value.get(0);

                //System.out.println(author);

                if(author.contains("/")){
                    author = gestoreRepo.getLastUriTerm(author);
                }

                query = gestoreRepo.askQuery(null, "hasAuthor", author);

                if(query.isEmpty()) {
                    req.setAttribute("Type","Author");
                    view = req.getRequestDispatcher("null_res.jsp");

                }
                else {

                    //for (Statement s : query) {
                       // System.out.println(s);
                    //}


                    gestoreRepo.getRdfFile(query,"Author");


                    HashMap out = gestoreRepo.getMapFromAuthorModel(query);

                    ArrayList<String> list = (ArrayList<String>) out.get("Publications");


                    //gestoreRepo.generateHTML("D:/IdeaProjectUltimate/ContentNegTest2/temp/temp.rdf");
                    //System.out.println(list);

                    req.setAttribute("QueryRes", out);
                    req.setAttribute("list", list);
                    view = req.getRequestDispatcher("query_res_aut.jsp");
                    //resp.sendRedirect("temp1.html");
                }
                break;

            }


        }
        gestoreRepo.closeRepo();

        view.forward(req, resp);



    }


}
