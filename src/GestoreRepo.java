import org.eclipse.rdf4j.model.*;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.eclipse.rdf4j.query.*;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.inferencer.fc.SchemaCachingRDFSInferencer;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;


public class GestoreRepo {

    private Repository rep = null;
    private String path = null;

    //Costruttore
    public GestoreRepo(String path) {


        //VERSIONE LOCALE
        //locazione della repository
        //File folder = new File("D:/IdeaProjectUltimate/ContentNegTest2/repository");
        //verifico che tale cartella esista
        //boolean exists = folder.exists();

        //Istanziazione di oggetti per la gestione della repo
        MemoryStore memoryStore = null;
        SchemaCachingRDFSInferencer schemaCachingRdfsInferencer = null;


        //se la cartella non esiste va creata e va creata anche la repo
        /*if (exists == false) {

            //Creating the directory
            boolean bool = folder.mkdir();
        }

        //creo la repo
        memoryStore = new MemoryStore(folder);
        memoryStore.setPersist(true);*/

        memoryStore = new MemoryStore();
        schemaCachingRdfsInferencer = new SchemaCachingRDFSInferencer(
                memoryStore);

        rep = new SailRepository(schemaCachingRdfsInferencer);
        //inizializzo la repo
        rep.init();


        //carico il my-otology.rdf
        //File bibo = new File("D:/IdeaProjectUltimate/ContentNegTest2/web/my-ontology.rdf");

        this.path=path;

        File bibo = new File(path+"/web/my-ontology.rdf");
        //System.out.println(path+"/my-ontology.rdf");



        //definisco il base uri per l'ontologia fabio
        //con questo uri posso accedere alle triple dell'ontologia
        //dovrei cambiarlo?
        String baseURI = "http://purl.org/ontology/bibo/";

        //aggiungo il file my-ontology.rdf alla repo
        try (RepositoryConnection con = rep.getConnection()) {
            try {

                con.add(bibo, baseURI, RDFFormat.RDFXML);


            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                con.close();
            }
        }

    }

    /* ASK QUERY */

    //chiede una query n cui l'oggetto e' una risorsa (e non un literal)
    //posso variare su subj, obj e predicate
    public Model askQuery(String subject, String predicate, String object) {

        //output della query
        Model queryRes = null;

        //uri utili
        String base = "http://purl.org/ontology/bibo/";

        //analizzo i parametri del metodo e faccio la query
        try (RepositoryConnection repCon = rep.getConnection()) {

            IRI subj = null;
            IRI pred = null;
            IRI obj = null;

            //se il soggetto non e' nullo allora creo un uri/iri
            if (subject != null) {
                String s = null;
                s = base + getIRICase(subject);
                subj = repCon.getValueFactory().createIRI(s);
            }

            //se il predicato non e' nullo allora creo un uri/iri
            if (predicate != null) {
                String p = null;
                if(predicate.equals("a")){
                    pred = RDF.TYPE;
                }
                else {
                    p = base + getIRICase(predicate);
                    pred = repCon.getValueFactory().createIRI(p);
                }
            }

            //se l'oggetto non e' nullo allora creo un uri/iri
            if (object != null) {
                String o = null;
                o = base + getIRICase(object);
                obj = repCon.getValueFactory().createIRI(o);
            }

            //true per inferenza
            queryRes = QueryResults.asModel(repCon.getStatements(
                    subj,
                    pred,
                    obj,
                    false));

            repCon.close();


        }

        //resittuisco il modello generato dalla query
        return queryRes;

    }


    /* INSERT STATEMENT */

    public void insertStatement(String subject, String predicate, String object) throws IOException {

        //uri utili
        String base = "http://purl.org/ontology/bibo/";

        ValueFactory vf = SimpleValueFactory.getInstance();

        IRI s = null;
        IRI p = null;
        IRI o = null;

        try(RepositoryConnection repCon = rep.getConnection()) {

            if (subject != null) {
                s = vf.createIRI(base, getIRICase(subject) );
            }

            if (predicate != null) {
                p = vf.createIRI( base, predicate );
            } else {
                p = RDF.TYPE;
            }

            if (object != null) {
                o = vf.createIRI( base, getIRICase(object));
            }


            //creo il modello nel quale metterò gli statement
            Model model = new LinkedHashModel();
            //indico il file dove voglio scrivere
            //FileOutputStream out = new FileOutputStream("D:\\IdeaProjectUltimate\\ContentNegTest2\\web\\temp.rdf");  // alla fine il file si troverà in questo percorso
            FileOutputStream out = new FileOutputStream(path+"/web/temp.rdf");  // alla fine il file si troverà in questo percorso

            //creo il writer che mi permetterà di scrivere
            RDFWriter writer = Rio.createWriter(RDFFormat.RDFXML, out);

            //file dell'ontologia
            String ontology = path+"/web/my-ontology.rdf";
            String ontology_out = path+"/out/artifacts/IA2CourseProject/my-ontology.rdf";

            model.add(s,p,o);

            //scrivo sul file le triple nel modello
            try {
                writer.startRDF();
                for (Statement st : model) {
                    writer.handleStatement(st);
                }
                writer.endRDF();
            } catch (RDFHandlerException e) {
                e.printStackTrace();
            }
            finally {
                out.close();
            }



            //scrivo sull'ontologia non deployata
            RandomAccessFile raf = new RandomAccessFile(ontology, "rw");
            long length = raf.length();
            raf.setLength(length - 12);
            raf.close();

            BufferedReader br = new BufferedReader(new FileReader(path+"/web/temp.rdf"));
            Writer output = new BufferedWriter(new FileWriter(ontology, true));

            String line;
            int i =0;
            while ((line = br.readLine()) != null) {
                if(i>2) {
                    output.write(line+"\n");
                }
                i+=1;
            }
            output.close();
            br.close();




            //scrivo sull'ontologia deployata
            RandomAccessFile raf1 = new RandomAccessFile(ontology_out, "rw");
            long length1 = raf1.length();
            raf1.setLength(length1 - 12);
            raf1.close();

            BufferedReader br1 = new BufferedReader(new FileReader(path+"/web/temp.rdf"));
            Writer output1 = new BufferedWriter(new FileWriter(ontology_out, true));

            String line1;
            int i1 =0;
            while ((line1 = br1.readLine()) != null) {
                if(i1>2) {
                    output1.write(line1+"\n");
                }
                i1+=1;
            }
            output1.close();
            br1.close();







            /*

            // Preparo un update per aggiungere i dati usando SPARQL 1.1
            Update update = repCon.prepareUpdate("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                    + "INSERT DATA {\n"
                    + subject + " " + predicate + " " + object + ".\n" + "}");
            // eseguo l'update
            update.execute();
            repCon.close();

             */

        }




    }

    /* INSERT DATA */

    public void insertData(String subject, String predicate, String object ) throws IOException {

        //uri utili
        String base = "http://purl.org/ontology/bibo/";

        ValueFactory vf = SimpleValueFactory.getInstance();

        IRI s = null;
        IRI p = null;
        Literal o = null;

        try(RepositoryConnection repCon = rep.getConnection()) {

            if (subject != null) {
                s = vf.createIRI(base, getIRICase(subject) );
            }

            if (predicate != null) {

                if(predicate.equals("hasYear")) {
                    p = vf.createIRI( base, predicate );
                    if (object != null) {
                        o = vf.createLiteral(object, XMLSchema.GYEAR);
                    }
                }
                else{
                    p = vf.createIRI( base, predicate );

                    if(object!=null){
                        o = vf.createLiteral( getIRICase(object), XMLSchema.STRING );
                    }
                }

            }

            //creo il modello nel quale metterò gli statement
            Model model = new LinkedHashModel();
            //indico il file dove voglio scrivere
            FileOutputStream out = new FileOutputStream(path+"/web/temp.rdf");  // alla fine il file si troverà in questo percorso
            //creo il writer che mi permetterà di scrivere
            RDFWriter writer = Rio.createWriter(RDFFormat.RDFXML, out);

            //file dell'ontologia
            String ontology = path+"/web/my-ontology.rdf";
            String ontology_out = path+"/out/artifacts/IA2CourseProject/my-ontology.rdf";

            model.add(s,p,o);

            //scrivo sul file le triple nel modello
            try {
                writer.startRDF();
                for (Statement st : model) {
                    writer.handleStatement(st);
                }
                writer.endRDF();
            } catch (RDFHandlerException e) {
                e.printStackTrace();
            }
            finally {
                out.close();
            }

            //scrivo sull-ontologia normale
            RandomAccessFile raf = new RandomAccessFile(ontology, "rw");
            long length = raf.length();
            raf.setLength(length - 12);
            raf.close();

            BufferedReader br = new BufferedReader(new FileReader(path+"/web/temp.rdf"));
            Writer output = new BufferedWriter(new FileWriter(ontology, true));

            String line;
            int i =0;
            while ((line = br.readLine()) != null) {
                if(i>2) {
                    output.write(line+"\n");
                }
                i+=1;
            }
            output.close();
            br.close();

            //scrivo sull-ontologia deployata
            RandomAccessFile raf1 = new RandomAccessFile(ontology_out, "rw");
            long length1 = raf1.length();
            raf1.setLength(length1 - 12);
            raf1.close();

            BufferedReader br1 = new BufferedReader(new FileReader(path+"/web/temp.rdf"));
            Writer output1 = new BufferedWriter(new FileWriter(ontology_out, true));

            String line1;
            int i1 =0;
            while ((line1 = br1.readLine()) != null) {
                if(i1>2) {
                    output1.write(line1+"\n");
                }
                i1+=1;
            }
            output1.close();
            br1.close();




            /*
            // Preparo un update per aggiungere i dati usando SPARQL 1.1
            Update update = repCon.prepareUpdate("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                    + "INSERT DATA {\n"
                    + subject + " " + predicate + " " + object + ".\n" + "}");
            // eseguo l'update
            update.execute();
            repCon.close();

             */

        }

    }

    /* GET RDF FILE */

    public void getRdfFile(Model model, String type) throws IOException {
        FileOutputStream out = null;
        FileOutputStream out1 = null;
        if(type.equals("Publication")) {
            //file temporaneo che mostreremo una volta convertito in html
            out = new FileOutputStream(path + "/web/query_res.rdf");
            out1 = new FileOutputStream(path+"/out/artifacts/IA2CourseProject/query_res.rdf");
        }
        else if(type.equals("Author")){
            out = new FileOutputStream(path + "/web/query_res_aut.rdf");
            out1 = new FileOutputStream(path+"/out/artifacts/IA2CourseProject/query_res_aut.rdf");
        }
        else if(type.equals("Complete")){
            out = new FileOutputStream(path + "/web/PublicationList.rdf");
            out1 = new FileOutputStream(path+"/out/artifacts/IA2CourseProject/PublicationList.rdf");
        }

        RDFWriter writer = Rio.createWriter(RDFFormat.RDFXML, out);

        try {
            Rio.write(model, out, RDFFormat.RDFXML);
            Rio.write(model, out1, RDFFormat.RDFXML);
        }
        finally {
            out.close();
            out1.close();
        }

    }


    /* GET DATA FROM MODEL */

    public HashMap getMapFromModel(Model model){

        HashMap out = new HashMap();

        out.put("Name", null);
        out.put("Type", null);
        out.put("hasAuthor", null);
        out.put("hasYear", null);
        out.put("hasBooktitle", null);
        out.put("hasJournal", null);

        String bibo = "http://purl.org/ontology/bibo/";
        model.setNamespace("bibo", bibo);

        //value factory (dovrei metterla come variabile globale/campo??)
        ValueFactory vf = SimpleValueFactory.getInstance();

        IRI hasAuthor = vf.createIRI(bibo, "hasAuthor");
        IRI academicArticle = vf.createIRI(bibo, "AcademicArticle");
        IRI proceedings = vf.createIRI(bibo, "Proceedings");
        IRI inproceedings = vf.createIRI(bibo, "Inproceedings");
        IRI conference = vf.createIRI(bibo, "Conference");
        IRI hasYear = vf.createIRI(bibo, "hasYear");
        IRI hasJournal = vf.createIRI(bibo, "hasJournal");
        IRI hasBooktitle = vf.createIRI(bibo, "hasBooktitle");

        ArrayList<String> authorList = new ArrayList<>();


        for (Statement statement: model){

            if(statement.getPredicate().equals(RDF.TYPE) && (statement.getObject().equals(academicArticle) ||
                    statement.getObject().equals(proceedings) || statement.getObject().equals(inproceedings) ||
                    statement.getObject().equals(conference)) ) {
                out.put("Name", (statement.getSubject().stringValue()));
                out.put("Type", statement.getObject().stringValue());
            }
            else if(statement.getPredicate().equals(hasAuthor)){
                authorList.add(statement.getObject().stringValue());
            }
            else if(statement.getPredicate().equals(hasYear)){
                out.put("hasYear", statement.getObject().stringValue());
            }
            else if(statement.getPredicate().equals(hasJournal)){
                out.put("hasJournal", statement.getObject().stringValue());
            }
            else if(statement.getPredicate().equals(hasBooktitle)){
                out.put("hasBooktitle", statement.getObject().stringValue());
            }


        }

        out.put("hasAuthor", authorList);

        return out;

    }

    public HashMap getMapFromAuthorModel(Model model){

        HashMap out = new HashMap();
        ArrayList<String> list = new ArrayList();

        out.put("Name", null);
        out.put("Publications", null);



        String bibo = "http://purl.org/ontology/bibo/";
        model.setNamespace("bibo", bibo);

        //value factory (dovrei metterla come variabile globale/campo??)
        ValueFactory vf = SimpleValueFactory.getInstance();

        IRI hasAuthor = vf.createIRI(bibo, "hasAuthor");

        for(Statement statement: model){
            list.add(statement.getSubject().toString());
        }

        for(Statement statement: model){
            out.put("Name",statement.getObject().stringValue());
        }



        out.put("Publications",list);



        return out;

    }

    /* GET MODEL FROM QUERY */

    //funzione che, data una query sottoforma di string restituisce il modello risultante
    public Model getModelFromQuery(String queryString){

        TupleQuery query = this.getRep().getConnection().prepareTupleQuery(queryString);

        ArrayList<String> out = new ArrayList<String>();

        try (TupleQueryResult result = query.evaluate()) {
            while (result.hasNext()) {
                BindingSet bindingSet = result.next();
                String subject = bindingSet.getValue("pub").toString();
                out.add(subject);

            }
        }

        Model model = null;
        ModelBuilder modelBuilder = new ModelBuilder();
        modelBuilder.setNamespace("bibo", "http://purl.org/ontology/bibo/");

        for(String s : out){
            String subj = s.substring(s.lastIndexOf('/')+1);
            model = this.askQuery(subj,null,null);
            for(Statement statement: model){
                modelBuilder.add(statement.getSubject(), statement.getPredicate(),statement.getObject());
            }

        }

        Model model_out = modelBuilder.build();
        return model_out;

    }

    /* GENERATE HTML FROM RDF */

    //DEPRECATED
    public void generateHTMLAuthor(String path) throws IOException {


        FileOutputStream outHtml = new FileOutputStream(path+"/web/temp1.html");
        Writer writer = new FileWriter(path+"/web/temp1.html");
        BufferedWriter bw = new BufferedWriter(writer);

        bw.write("<!DOCTYPE html>"+"\n");
        bw.write("<html lang=\"en\">"+"\n");
        bw.write("<head>"+"\n");
        bw.write("<meta charset=\"UTF-8\">"+"\n");

        bw.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"query_aut_style.css\">"+"\n");
        //che titolo metto?
        bw.write("<title>Query Result</title>"+"\n");
        bw.write("</head>"+"\n");
        bw.write("<body>"+"\n");

        bw.write("<div class=\"topnav\">"+"\n");
        bw.write("<a href=\"index.html\">Home</a>"+"\n");
        bw.write("<a href=\"update.html\">Update</a>"+"\n");
        bw.write("<a href=\"query.html\">Query</a>"+"\n");
        bw.write("</div>"+"\n");

        bw.write("<h1>"+"\n");
        bw.write("Query Results"+"\n");
        bw.write("</h1>"+"\n");

        InputStream is = new FileInputStream(path);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine();

        String[] app;
        String[] app1;


        while(line != null) {
            if (line.contains("<rdf:Description")) {
                app = line.split(" ");
                app1 = app[app.length - 1].split("/");
                String name = app1[app1.length - 1];
                name = name.substring(0, name.length() - 2);

                bw.write("<dl>\n");

                bw.write("<dt>\n");

                bw.write("<div>" + "\n");

                bw.write("<form method=\"post\" action=\"Query\">\n");
                bw.write("<label>\n");
                bw.write("<input name=\"hasAuthor\" value=\"" + name + "\" style=\"display:none;\">\n");
                bw.write("<input type=\"submit\" value=" + name + ">");
                bw.write("</label>\n");
                bw.write("</form>\n");
                bw.write("</dt>\n");
                //bw.write("<br><br>\n");


            }


            line = buf.readLine();
        }

        bw.write("</dl>\n");
        bw.write("</div>"+"\n");



        bw.write("</body>"+"\n");
        bw.write("</html>"+"\n");

        bw.close();
        writer.close();


    }

    //DEPRECATED
    public void generateHTMLPub(String path) throws IOException {

        /*String out = "/temp_out";


        InputStream is = new FileInputStream(path);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine(); StringBuilder sb = new StringBuilder();
        while(line != null){
            sb.append(line).append("\n");
            line = buf.readLine(); }
        String fileAsString = sb.toString();

        RDF2HTML rdf2html = new RDF2HTML(fileAsString, out);

        rdf2html.performTask();

         */

        FileOutputStream outHtml = new FileOutputStream(path+"/web/temp.html");
        Writer writer = new FileWriter(path+"/web/temp.html");
        BufferedWriter bw = new BufferedWriter(writer);

        bw.write("<!DOCTYPE html>"+"\n");
        bw.write("<html lang=\"en\">"+"\n");
        bw.write("<head>"+"\n");
        bw.write("<meta charset=\"UTF-8\">"+"\n");

        bw.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"query_aut_style.css\">"+"\n");
        //che titolo metto?
        bw.write("<title>Query Result</title>"+"\n");
        bw.write("</head>"+"\n");
        bw.write("<body>"+"\n");

        bw.write("<div class=\"topnav\">"+"\n");
        bw.write("<a href=\"index.html\">Home</a>"+"\n");
        bw.write("<a href=\"update.html\">Update</a>"+"\n");
        bw.write("<a href=\"query.html\">Query</a>"+"\n");
        bw.write("</div>"+"\n");

        bw.write("<h1>"+"\n");
        bw.write("Query Results"+"\n");
        bw.write("</h1>"+"\n");

        InputStream is = new FileInputStream(path);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine();

        String[] app;
        String[] app1;


        while(line != null){
            if(line.contains("<rdf:Description")){
                app = line.split(" ");
                app1 = app[app.length-1].split("/");
                String name = app1[app1.length-1];
                name = name.substring(0,name.length()-2);

                bw.write("<dl>\n");



                bw.write("<div"+"\n");

                bw.write("<dt>\n");

                bw.write("<label>\n");
                bw.write(name+"\n");
                bw.write("</label>"+"\n");


                bw.write("</dt>\n");
                //bw.write("<br><br>\n");

                //REMEMBER
                bw.write("<div id=\"resp-table-body\">");

            }
            else if (line.contains("<rdf:type")){

                //System.out.println(line);

                app = line.split(" ");
                app1 = app[app.length-1].split("/");
                String type = app1[app1.length-2];
                if (! type.equals("rdf-schema#Resource\"")){
                    type = type.substring(0,type.length()-1);

                    bw.write("<dd>\n");

                    //bw.write("<div>\n");



                    bw.write("<div class=\"resp-table-row\">\n");

                    bw.write("<form method=\"post\" action=\"Query\">\n");
                    bw.write("<label>\n");
                    bw.write("<input name=\"a\" value=\""+type+ "\" style=\"display:none;\">\n");
                    bw.write("a ");
                    bw.write("<input type=\"submit\" value="+type+ ">");
                    bw.write("</label>\n");
                    bw.write("</form>\n");
                    bw.write("</div>\n");



                    bw.write("</dd>\n");
                    //bw.write("a "+type+"\n");

                    //bw.write("<br><br>\n");
                }


            }

            else if(line.contains("<hasAuthor")){

                //REMEMBER
                bw.write("</div>\n");

                app = line.split(" ");
                app1 = app[app.length-1].split("/");
                String author = app1[app1.length-2];
                author = author.substring(0,author.length()-1);

                bw.write("<dd>\n");
                bw.write("<div>\n");
                bw.write("<form method=\"post\" action=\"Query\">\n");
                bw.write("<label>\n");
                bw.write("<input name=\"hasAuthor\" value=\""+author+ "\" style=\"display:none;\">\n");
                bw.write("hasAuthor ");
                bw.write("<input type=\"submit\" value="+author+ ">");
                bw.write("</label>\n");
                bw.write("</form>\n");
                bw.write("</div>\n");
                bw.write("</dd>\n");


                //bw.write("hasAuthor "+author+"\n");


                //bw.write("<br><br>\n");

            }

            else if(line.contains("<hasYear")){

                app = line.split(" ");
                app1 = app[app.length-1].split("/");
                String year = app1[app1.length-2];
                year = year.substring(0,year.length()-1);

                bw.write("<dd>\n");
                bw.write("<div>\n");
                bw.write("<form method=\"post\" action=\"Query\">\n");
                bw.write("<label>\n");
                bw.write("<input name=\"hasYear\" value=\""+year+ "\" style=\"display:none;\">\n");
                bw.write("hasYear ");
                bw.write("<input type=\"submit\" value="+year+ ">");
                bw.write("</label>\n");
                bw.write("</form>\n");
                bw.write("</div>\n");
                bw.write("</dd>\n");


                //bw.write("hasYear "+year+"\n");

                //bw.write("<br><br>\n");

            }

            else if(line.contains("<hasBooktitle")){

                app = line.split(" ");
                app1 = app[app.length-1].split("/");
                String bt = app1[app1.length-2];
                bt = bt.substring(0,bt.length()-1);

                bw.write("<dd>\n");
                bw.write("<div>\n");
                bw.write("<form method=\"post\" action=\"Query\">\n");
                bw.write("<label>\n");
                bw.write("<input name=\"hasBooktitle\" value=\""+bt+ "\" style=\"display:none;\">\n");
                bw.write("hasBooktitle ");
                bw.write("<input type=\"submit\" value="+bt+ ">");
                bw.write("</label>\n");
                bw.write("</form>\n");
                bw.write("</div>\n");
                bw.write("</dd>\n");


                //bw.write("hasBooktitle "+bt+"\n");


                //bw.write("<br><br>\n");

            }

            else if(line.contains("<hasJournal")){

                app = line.split(" ");
                app1 = app[app.length-1].split("/");
                String j = app1[app1.length-2];
                j = j.substring(0,j.length()-1);

                bw.write("<dd>\n");
                bw.write("<div>\n");
                bw.write("<form method=\"post\" action=\"Query\">\n");
                bw.write("<label>\n");
                bw.write("<input name=\"hasJournal\" value=\""+j+ "\" style=\"display:none;\">\n");
                bw.write("hasJournal ");
                bw.write("<input type=\"submit\" value="+j+ ">");
                bw.write("</label>\n");
                bw.write("</form>\n");
                bw.write("</div>\n");
                bw.write("</dd>\n");

                //bw.write("hasJournal "+j+"\n");


                //bw.write("<br><br>\n");

            }

            line = buf.readLine();
        }
        bw.write("</dl>\n");
        bw.write("</div>"+"\n");



        bw.write("</body>"+"\n");
        bw.write("</html>"+"\n");

        bw.close();
        writer.close();
        //outHtml.close();



    }

    /*
    public void generateHTMLFromOntology() throws IOException {

        FileOutputStream outHtml = new FileOutputStream(path+"/out/artifacts/ContentNegTest2_war_exploded/PublicationList.html");
        Writer writer = new FileWriter(path+"/out/artifacts/ContentNegTest2_war_exploded/PublicationList.html");
        BufferedWriter bw = new BufferedWriter(writer);

        bw.write("<!DOCTYPE html>"+"\n");
        bw.write("<html lang=\"en\">"+"\n");
        bw.write("<head>"+"\n");
        bw.write("<meta charset=\"UTF-8\">"+"\n");

        bw.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"pubstyle.css\">"+"\n");

        //che titolo metto?
        bw.write("<title>Publication List</title>"+"\n");
        bw.write("</head>"+"\n");
        bw.write("<body>"+"\n");

        bw.write("<div class=\"topnav\">"+"\n");
        bw.write("<a href=\"index.html\">Home</a>"+"\n");
        bw.write("<a href=\"update.html\">Update</a>"+"\n");
        bw.write("<a href=\"query.html\">Query</a>"+"\n");
        bw.write("</div>"+"\n");

        bw.write("<h1>"+"\n");
        bw.write("Publication List"+"\n");
        bw.write("</h1>"+"\n");

        InputStream is = new FileInputStream(path+"/out/artifacts/ContentNegTest2_war_exploded/PublicationList.rdf");
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine();

        String[] app;
        String[] app1;



        while(line != null){
            if(line.contains("<rdf:Description")){
                app = line.split(" ");
                app1 = app[app.length-1].split("/");
                String name = app1[app1.length-1];
                name = name.substring(0,name.length()-2);








                bw.write("<div style=\"display:flex; flex-direction: row;  align-items: center\">"+"\n");

                bw.write("<form method=\"post\" action=\"Query\">\n");

                bw.write("<p class=\"input\">\n");

                bw.write("<input name=\"Publication\" value=\""+name+ "\" style=\"display:none !important;\">\n");
                bw.write("<input type=\"submit\" value="+name+ ">");


                bw.write("</p>\n");
                bw.write("</form>\n");
                line=buf.readLine();
                while(! line.equals("</rdf:Description>")) {

                    if (line.contains("<bibo:hasAuthor")) {
                        app = line.split(" ");
                        app1 = app[app.length - 1].split("/");
                        String author = app1[app1.length - 2];
                        System.out.println(author);
                        author = author.substring(0, author.length() - 1);
                        bw.write("<form method=\"post\" action=\"Query\">\n");

                        bw.write("<p class=\"input\">\n");

                        bw.write("<input name=\"hasAuthor\" value=\"" + author + "\" style=\"display:none !important;\">\n");
                        bw.write("<input type=\"submit\" value=" + author + ">");


                        bw.write("</p>\n");
                        bw.write("</form>\n");
                    }

                    line = buf.readLine();

                    if (line.contains("<rdf:type")) {
                        app = line.split(" ");
                        app1 = app[app.length - 1].split("/");
                        String pubType = app1[app1.length - 2];
                        pubType = pubType.substring(0, pubType.length() - 1);


                        bw.write("<p class=\"input\">\n");


                        bw.write(pubType + "\n");


                        bw.write("</p>\n");

                    }


                    line = buf.readLine();


                }
                bw.write("</div>\n");




            }


            line = buf.readLine();
        }


        bw.write("</body>"+"\n");
        bw.write("</html>"+"\n");

        bw.close();
        writer.close();
        //outHtml.close();




    }*/

    public void generateHTMLFromOntology(Model model) throws IOException {

        TreeMap map = new TreeMap();
        HashMap app = new HashMap();

        String name = null;
        String type = null;
        String author = null;
        String year = null;
        String[] split = null;

        String bibo = "http://purl.org/ontology/bibo/";
        model.setNamespace("bibo", bibo);

        //value factory (dovrei metterla come variabile globale/campo??)
        ValueFactory vf = SimpleValueFactory.getInstance();

        IRI hasAuthor = vf.createIRI(bibo, "hasAuthor");
        IRI hasYear = vf.createIRI(bibo, "hasYear");

        ArrayList<String> author_list = null;

        for (Statement s : model) {


            split = s.getSubject().toString().split("/");
            name = split[split.length - 1];

            //System.out.println(s.toString());

            if (!map.containsKey(name)) {
                author_list = new ArrayList<>();
                author_list.add("");
                app = new HashMap();
                map.put(name, app);
                app.put("Year","");
            }


            if (s.getPredicate().equals(hasAuthor)) {
                split = s.getObject().toString().split("/");
                author = split[split.length - 1];
                author_list.add(author);
            } else if (s.getPredicate().equals(RDF.TYPE)) {
                split = s.getObject().toString().split("/");
                type = split[split.length - 1];
                if(!type.equals("Agent")) {
                    app.put("Type", type);
                }
            }
            else if(s.getPredicate().equals(hasYear)){


                split = s.getObject().toString().split(">");
                year = split[split.length-1];
                if(year.length()>0) {
                    //year = year.substring(1, 5);
                }
                //System.out.println(year);
                app.put("Year", year);
            }
            app.put("Author", author_list);


        }

        FileOutputStream outHtml = new FileOutputStream(path + "/out/artifacts/IA2CourseProject/PublicationList.html");
        Writer writer = new FileWriter(path + "/out/artifacts/IA2CourseProject/PublicationList.html");
        BufferedWriter bw = new BufferedWriter(writer);

        bw.write("<!DOCTYPE html>" + "\n");
        bw.write("<html lang=\"en\">" + "\n");
        bw.write("<head>" + "\n");
        bw.write("<meta charset=\"UTF-8\">" + "\n");

        bw.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"pubstyle.css\">" + "\n");

        //che titolo metto?
        bw.write("<title>Publication List</title>" + "\n");
        bw.write("</head>" + "\n");
        bw.write("<body>" + "\n");

        bw.write("<div class=\"topnav\">" + "\n");
        bw.write("<a href=\"index.html\">Home</a>" + "\n");
        bw.write("<a href=\"update.html\">Update</a>" + "\n");
        bw.write("<a href=\"query.html\">Query</a>" + "\n");
        bw.write("</div>" + "\n");

        bw.write("<h1>" + "\n");
        bw.write("Publication List" + "\n");
        bw.write("</h1>" + "\n");

        //divs for table displaying
        bw.write("<div class=\"rTable\">\n");

        //divs for table heading
        bw.write("<div class=\"rTableRow\">\n");
        bw.write("<div class=\"rTableHead\"><span style=\"font-weight: bold;\">Publication Name</span></div>");
        bw.write("<div class=\"rTableHead\"><span style=\"font-weight: bold;\">Type</span></div>\n");
        bw.write("<div class=\"rTableHead\"><span style=\"font-weight: bold;\">Year</span></div>\n");
        bw.write("<div class=\"rTableHead\"><span style=\"font-weight: bold;\">Author</span></div>\n");
        bw.write("</div>\n");

        for (Object s : map.keySet()) {

            name = (String) s;

            //System.out.println(name);



            //riga iesima:
            bw.write("<div class=\"rTableRow\">\n");
            bw.write("<div class=\"rTableCell\">\n");



            bw.write("<form method=\"post\" action=\"Query\">\n");
            bw.write("<input name=\"Publication\" value=\"" + name + "\" style=\"display:none !important;\">\n");
            bw.write("<input type=\"submit\" value=" + name + ">");
            bw.write("</form>\n");
            bw.write("</div>\n");

            app = (HashMap) map.get(name);

            for (Object o : app.keySet()) {
                String object = (String) o;
                if (object.equals("Author")) {

                    author_list = (ArrayList<String>) app.get("Author");

                    bw.write("<div class=\"rTableCell\">\n");

                    for(String a : author_list) {



                        bw.write("<form method=\"post\" action=\"Query\">\n");

                        bw.write("<input name=\"hasAuthor\" value=\"" + a + "\" style=\"display:none !important;\">\n");
                        bw.write("<input type=\"submit\" value=" + a + ">");

                        bw.write("</form>\n");


                    }
                    bw.write("</div>\n");
                } else if (object.equals("Type")) {

                    type = (String) app.get("Type");
                    bw.write("<div class=\"rTableCell\">\n");

                    bw.write(type + "\n");

                    bw.write("</div>\n");

                } else if (object.equals("Year")) {

                    year = (String) app.get("Year");
                    //System.out.println(year);
                    if(year.length()>0) {
                        year = year.substring(1, 5);
                    }
                    bw.write("<div class=\"rTableCell\">\n");

                    bw.write(year + "\n");

                    bw.write("</div>\n");

                }


            }
            bw.write("</div>\n");

        }
        bw.write("</div>\n");
        bw.write("</body>"+"\n");
        bw.write("</html>"+"\n");

        bw.close();
        writer.close();
    }




    /* UTILITIES */


    public Repository getRep() {
        return rep;
    }

    public void setRep(Repository rep) {
        this.rep = rep;
    }

    //chiudo la repository
    public void closeRepo() {
        rep.shutDown();
    }

    //converto una stringa in IRI Case
    public String getIRICase(String s) {
        String[] subjects = s.split(" ");
        String subject = "";
        String tmp = "";
        if (subjects.length > 1) {
            for (int i = 0; i < subjects.length; i++) {
                if (subjects[i].length() > 1) {
                    tmp = subjects[i].substring(0, 1).toUpperCase() + subjects[i].substring(1, subjects[i].length()).toLowerCase();
                } else {
                    tmp = subjects[i].toUpperCase();
                }
                subject = subject + tmp;
            }
        } else {

            subject = subjects[0];

        }
        subject = subject.replaceAll("[^a-zA-Z0-9_-]", "");
        return subject;
    }

    //ottiene l-ultimo termine di un uri
    public String getLastUriTerm(String s){
        String out = null;

        String[] list = s.split("/");
        out = list[list.length-1];
        return out;
    }

    //ottiene una lista di tutti gli autori da inserire
    public String[] getAuthorsList(String s){

        String[] out = s.split(",");

        return out;
    }

}







