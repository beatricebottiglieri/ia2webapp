<%--
  Created by IntelliJ IDEA.
  User: beart
  Date: 6/3/2020
  Time: 1:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="query_aut_style.css">
    <title>Query Result</title>
</head>
<body>
<div class="topnav">
    <a href="index.html">Home</a>
    <a href="update.html">Update</a>
    <a href="query.html">Query</a>
</div>
<h1>
    Query Results
</h1>

<c:if test="${Type=='Publication'}">
    <c:out value="No Publication matches the query"></c:out>
</c:if>

<c:if test="${Type=='Author'}">
    <c:out value="No Author matches the query"></c:out>
</c:if>

</body>
</html>
